import { Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthController } from "./auth.controller";
import { AuthService } from "./auth.service";
import { JwtStrategy } from "./jwt.strategy";
import { LocalStrategy } from "./locacl.strategy";
import { User } from "./user.entity";
import { UserController } from "./users.controller";

@Module({
    imports: [TypeOrmModule.forFeature([User]),
    JwtModule.registerAsync({
        useFactory: () => ({
            secret: process.env.AUTH_SECRET,
            signOptions: {
                expiresIn: '60m'
            }
        })
    })
    ],
    providers: [LocalStrategy,JwtStrategy,AuthService],
    controllers: [AuthController,UserController]
})
export class AuthModule { }