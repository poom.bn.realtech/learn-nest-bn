import { Injectable, Logger } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateEventDto } from "learn-nest-bn/dist/create-event.dto";
import { UpdateEventDto } from "learn-nest-bn/dist/update-event.dto";
import { use } from "passport";
import { User } from "./../auth/user.entity";
import { paginate, PaginateOptions } from "./../pagination/paginator";
import { DeleteResult, Repository, SelectQueryBuilder } from "typeorm";
import { AttendeeAnswerEnum } from "./attendee.entity";
import { Event, PaginatedEvents } from "./event.entity";
import { ListEvents, WhenEventFilter } from "./list.events";

@Injectable()
export class EventsService {
    private readonly logger = new Logger(EventsService.name)
    constructor(
        @InjectRepository(Event)
        private readonly eventRepository: Repository<Event>
    ) { }
    private getEventsBaseQuery() : SelectQueryBuilder<Event>{
        return this.eventRepository.createQueryBuilder('e').orderBy('e.id', 'DESC');
    }
    public getEventsWithAttendeeCountQuery() : SelectQueryBuilder<Event>{
        return this.getEventsBaseQuery() .loadRelationCountAndMap(
            'e.attendeeCount', 'e.attendees'
          )
          .loadRelationCountAndMap(
            'e.attendeeAccepted',
            'e.attendees',
            'attendee',
            (qb) => qb
              .where(
                'attendee.answer = :answer',
                { answer: AttendeeAnswerEnum.Accepted }
              )
          )
          .loadRelationCountAndMap(
            'e.attendeeMaybe',
            'e.attendees',
            'attendee',
            (qb) => qb
              .where(
                'attendee.answer = :answer',
                { answer: AttendeeAnswerEnum.Maybe }
              )
          )
          .loadRelationCountAndMap(
            'e.attendeeRejected',
            'e.attendees',
            'attendee',
            (qb) => qb
              .where(
                'attendee.answer = :answer',
                { answer: AttendeeAnswerEnum.Rejected }
              )
          );
       
    }
    private getEventsWithAttendeeCountFilteredQuery(filter?: ListEvents) : SelectQueryBuilder<Event>{
        let query = this.getEventsWithAttendeeCountQuery();
        if (!filter) {
            return query;
        }
        if (filter.when) {
            if (filter.when == WhenEventFilter.Today) {
                query = query.andWhere(
                    'e.when >= CURDATE() AND e.when <= CURDATE() + INTERVAL 1 DAY'
                );

            }
            if (filter.when == WhenEventFilter.Tommorow) {
                query = query.andWhere(
                    'e.when >= CURDATE() + INTERVAL 1 DAY AND e.when <= CURDATE() + INTERVAL 2 DAY'
                );

            }
            if (filter.when == WhenEventFilter.ThisWeek) {
                query = query.andWhere('YEARWEEK(e.when,1) = YEARWEEK(CRUDATE(),1)');

            }
            if (filter.when == WhenEventFilter.NextWeek) {
                query = query.andWhere('YEARWEEK(e.when,1) = YEARWEEK(CRUDATE(),1)+1');

            }
        }
        return query;
    }
    public async getEventsWithAttendeeCountFilteredPaginated(
        filter: ListEvents,
        pageinateOptions: PaginateOptions
    ):Promise<PaginatedEvents> {
       return await paginate(
           await this.getEventsWithAttendeeCountFilteredQuery(filter),
           pageinateOptions
       );
    }
    public async getEventWithAttendeeCount(id: number): Promise<Event | undefined> {
        const query = await this.getEventsWithAttendeeCountQuery().andWhere('e.id = :id', { id });
        this.logger.debug(query.getSql());
        return await query.getOne();
    }
    public async createEvent(input:CreateEventDto,user:User):Promise<Event>{
        return await this.eventRepository.save(
            new Event({
                ...input,
                organizer:user,
                when:new Date(input.when),
            })
        );
    }

    public async findOne(id:number):Promise<Event | undefined>{
        return await this.eventRepository.findOne(id);
    }
    public async updateEvent(event:Event,input:UpdateEventDto):Promise<Event>{
        return await this.eventRepository.save(
            new Event ({
                ...event,
                ...input,
                when:input.when?new Date(input.when):event.when,
            })
        )
    }
    public async deleteEvent(id:number):Promise<DeleteResult>{
        return await this.eventRepository
        .createQueryBuilder('e')
        .delete()
        .where('id = :id',{id})
        .execute();
    }
    public async getEventsOranizedByUserIdPaginated(
        userID:number,
        pageinateOptions:PaginateOptions
    ):Promise<PaginatedEvents>{
        return await paginate<Event>(
            this.getEventsOrganizedByUserIdQuery(userID),
            pageinateOptions
        );
    }
    private getEventsOrganizedByUserIdQuery(
        userID:number
    ):SelectQueryBuilder<Event>{
        return this.getEventsBaseQuery().where('e.organizerId = userID' ,{ userID })
    }
    public async getEventsAttendedByUserIdPaginated(
        userID:number,
        pageinateOptions:PaginateOptions
    ):Promise<PaginatedEvents>{
        return await paginate<Event>(
            this.getEventsAttendedByUserIdQuery(userID),
            pageinateOptions
        );
    }
    private getEventsAttendedByUserIdQuery(
        userID:number
    ) : SelectQueryBuilder<Event>{
        return this.getEventsBaseQuery().leftJoinAndSelect('e.attendees','a')
        .where('a.userID = :userID',{userID});
    } 
}