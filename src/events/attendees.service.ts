import { Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Attendee } from "./attendee.entity";

@Injectable()
export class AttendeeService {
    constructor(
        @InjectRepository(Attendee)
        private readonly attendeeRepository:Repository<Attendee>
        ){}
        public async findByEventID(eventID:number):Promise<Attendee[]>{
            return await this.attendeeRepository.find({
                event:{id:eventID}
            });
        }
        public async findOneByEventUserID(eventID:number,userID:number):Promise<Attendee | undefined>{
            return await this.attendeeRepository.findOne({
                event:{id : eventID},
                user:{id:userID}
            })
        }
        public async createOrUpdate(input:any, eventID:number,userID:number):Promise<Attendee>
        {
            const attendee = await this.findOneByEventUserID(eventID,userID)
                ?? new Attendee();
                
                attendee.eventId = eventID;
                attendee.userId = userID;
                attendee.answer = input.answer;
                return await this.attendeeRepository.save(attendee);
        }
}       
    