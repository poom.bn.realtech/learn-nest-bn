import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Attendee } from './attendee.entity';
import { AttendeeService } from './attendees.service';
import { CurrentUserEventAttendanceController } from './current-user-event-attendance.controller';
import { EventAttendeesController } from './event-attendees.controller';
import { EventsOrganizedByUserController } from './event-organized-by-user.controller';
import { Event } from './event.entity';
import{EventsController} from './events.controller'
import { EventsService } from './events.service';


@Module({
    imports:[
        TypeOrmModule.forFeature([Event,Attendee])
    ],
    controllers:[
        EventsController,
        CurrentUserEventAttendanceController,
        EventAttendeesController,
        EventsOrganizedByUserController
    ],
    providers:[EventsService,AttendeeService]
   
})
export class EventsModule {}
