import { ClassSerializerInterceptor, Controller, Get, Param, ParseIntPipe, UseInterceptors } from "@nestjs/common";
import { Attendee } from "./attendee.entity";
import { AttendeeService } from "./attendees.service";

@Controller('events/:eventID/attendees')
export class EventAttendeesController{
    constructor(
        private readonly attendsService:AttendeeService
    ){}
    @Get()
    @UseInterceptors(ClassSerializerInterceptor)
    async findAll(@Param('eventID',ParseIntPipe) eventID:number){
        return await this.attendsService.findByEventID(eventID);
    }
}