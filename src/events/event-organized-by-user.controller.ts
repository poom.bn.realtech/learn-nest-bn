import { ClassSerializerInterceptor, Controller, DefaultValuePipe, Get, Param, ParseIntPipe, Query, SerializeOptions, UseInterceptors } from "@nestjs/common";
import { EventsService } from "./events.service";

@Controller('event-organized-by-user/:userID')
@SerializeOptions({strategy:'excludeAll'})
export class EventsOrganizedByUserController{
    constructor(
        private readonly eventsService:EventsService,
    ){

    }
    @Get()
    @UseInterceptors(ClassSerializerInterceptor)
    async findAll(@Param('userID',ParseIntPipe) userID:number,
    @Query('page',new DefaultValuePipe(1),ParseIntPipe) page=1
    ){
        return await this.eventsService.getEventsOranizedByUserIdPaginated(userID
            ,{currentPage:page,limit:5});
    }
}