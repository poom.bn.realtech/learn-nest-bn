import { Expose } from "class-transformer";
import { User } from "./../auth/user.entity";
import { PaginationResult } from "./../pagination/paginator";
import {Column, Entity, JoinColumn, ManyToOne, OneToMany, PrimaryGeneratedColumn} from "typeorm";
import{Attendee} from "./attendee.entity";
@Entity()
export class Event{
    constructor(partial?: Partial<Event>)
    {
        Object.assign(this,partial);
        
    }
    
    @PrimaryGeneratedColumn()
    @Expose()
    id:number;
    @Column({length:100})
    @Expose()
    name:string;
    @Column()
    @Expose()
    description:string;
    @Column({name:'when'})
    @Expose()
    when:Date;
    @Column()
    @Expose()
    address:string;
    @OneToMany(() => Attendee,(attendee) => attendee.event,{
        eager:true
    })
    @Expose()
    attendees:Attendee[];
    @Expose()
    attendeeCount?:number;
    @Expose()
    attendeeRejected?:number;
    @Expose()
    attendeeMaybe?:number;
    @Expose()
    attendeeAccepted?:number;
    

    @ManyToOne(() => User,(user) => user.organized)
    @JoinColumn({name:'organizerId'})
    @Expose()
    organizer: User;
    @Column({nullable:true})
    organizerId:number;
}
export type PaginatedEvents = PaginationResult<Event>;