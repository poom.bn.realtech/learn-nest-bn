import { Body, ClassSerializerInterceptor, Controller, DefaultValuePipe, Get, NotFoundException, Param, ParseIntPipe, Put, Query, SerializeOptions, UseGuards, UseInterceptors } from "@nestjs/common";
import { AuthGuardJwt } from "./../auth/auth-guard-jwt";
import { CurrentUser } from "./../auth/current-user.decorator";
import { CreateAttendeeDto } from "./../auth/input/create-attendee.dto";
import { User } from "./../auth/user.entity";
import { AttendeeService } from "./attendees.service";
import { EventsService } from "./events.service";

@Controller('events-attenance')
@SerializeOptions({strategy:'excludeAll'})
export class CurrentUserEventAttendanceController{
    constructor(
        private readonly eventsService:EventsService,
        private readonly attendeesService:AttendeeService
    ){

    }
    @Get()
    @UseGuards(AuthGuardJwt)
    @UseInterceptors(ClassSerializerInterceptor)
    async findAll(
        @CurrentUser() user:User,
        @Query('page',new DefaultValuePipe(1),ParseIntPipe) page = 1
    ){
        return await this.eventsService.getEventsAttendedByUserIdPaginated(user.id,{limit:6,currentPage:page});
    }
    @Get(':eventID')
    @UseGuards(AuthGuardJwt)
    @UseInterceptors(ClassSerializerInterceptor)
    async findOne(
        @Param('eventID',ParseIntPipe)eventID:number,
        @CurrentUser() user:User
    ){
        const attendee = await this.attendeesService.findOneByEventUserID(eventID,user.id);
        if(!attendee){
            throw new NotFoundException()
        }
        return attendee;
    }
    @Put('/:eventID')
    @UseGuards(AuthGuardJwt)
    @UseInterceptors(ClassSerializerInterceptor)
    async createOrUpdate(
        @Param('eventID',ParseIntPipe) eventID:number,
        @Body()input:CreateAttendeeDto,
        @CurrentUser()user:User
    ){
        return this.attendeesService.createOrUpdate(input,eventID,user.id);
    }
}