import { Body, ClassSerializerInterceptor, Controller,Delete,ForbiddenException,Get, HttpCode, Logger, NotFoundException, Param, ParseIntPipe, Patch, Post, Query, SerializeOptions, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Like, MoreThan, Repository } from "typeorm";
import { CreateEventDto } from "./create-event.dto";
import { Event } from "./event.entity";
import { UpdateEventDto } from "./update-event.dto";
import { EventsService } from "./events.service";
import { ListEvents } from "./list.events";
import { CurrentUser } from "./../auth/current-user.decorator";
import { User } from "./../auth/user.entity";
import { AuthGuardJwt } from "./../auth/auth-guard-jwt";
@Controller('/event')
@SerializeOptions({strategy:'excludeAll'})
export class EventsController{
    private readonly logger = new Logger(EventsController.name);
    constructor(
        private readonly eventsService: EventsService
    ){}
    private events:Event[] = [];
    @Get()
    @UsePipes(new ValidationPipe({ transform: true }))
    @UseInterceptors(ClassSerializerInterceptor)
    async findAll(@Query() filter: ListEvents) {
      const events = await this.eventsService
        .getEventsWithAttendeeCountFilteredPaginated(
          filter,
          {
            total: true,
            currentPage: filter.page,
            limit: 2
          }
        );
      return events;
    }
    
    // @Get('/practice2')
    // async practice2(){
    //     return await this.repository.findOne(1);
    // }
    // @Get('/practice')
    // async practice(){
    //     return await this.repository.find({
    //         select:['id','when'],
    //         where:[{
    //             id:MoreThan(3),
    //             when:MoreThan(new Date('2021-02-12T13:00:00'))
    //         },{
    //             description:Like('%meet%')
    //         }],
    //         take:2,
    //         order:{
    //             id:'DESC'
    //         }
    //     });
    // }
    @Get(':id')
    @UseInterceptors(ClassSerializerInterceptor)
    async findOne(@Param('id',ParseIntPipe) id : number){
        //console.log(typeof id);
        const event = await this.eventsService.getEventWithAttendeeCount(id);
        if(!event){
            throw new NotFoundException();
        }
        return event
    }
    @Post()
    @UseGuards(AuthGuardJwt)
    @UseInterceptors(ClassSerializerInterceptor) @UseInterceptors(ClassSerializerInterceptor)
    async create(@Body(ValidationPipe) input : CreateEventDto,
    @CurrentUser()user:User
    ){
        return await this.eventsService.createEvent(input,user)

    }

    @Patch(':id')
    @UseGuards(AuthGuardJwt)
    @UseInterceptors(ClassSerializerInterceptor)
    async update(@Param('id',ParseIntPipe) id,@Body() 
    input:UpdateEventDto,
    @CurrentUser()user:User

    ){
        const event = await this.eventsService.findOne(id);
      if(!event){
          throw new NotFoundException;
      }
      if(event.organizerId !== user.id){
          throw new ForbiddenException(null,'You not authorized to change event')
      }

     return await this.eventsService.updateEvent(event,input);
    }
   
    @Delete(':id')
    @UseGuards(AuthGuardJwt)
    @HttpCode(204)
    async remove(@Param('id',ParseIntPipe) id, @CurrentUser()user : User){
        const event = await this.eventsService.findOne(id);
        if (!event) {
            throw new NotFoundException();
          }
      
          if (event.organizerId !== user.id) {
            throw new ForbiddenException(
              null, `You are not authorized to remove this event`
            );
          }
         await this.eventsService.deleteEvent(id);
        
    }
    
}