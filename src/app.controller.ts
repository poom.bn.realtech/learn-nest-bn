import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('/hello')
  getHello01(): string {
    return "test";
  }
  @Post('/post')
  getHello02():string{
    return "testpost";
  }
}
